---
title: duud static site
---

# duud static
{: .glitch data-text="duud static" }

[View project](https://gitlab.com/duud/static)  
[Read middleman documentation](https://middlemanapp.com/basics/templating_language/)
